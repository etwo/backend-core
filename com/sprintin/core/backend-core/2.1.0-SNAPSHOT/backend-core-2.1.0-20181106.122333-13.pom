<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.sprintin.core</groupId>
  <artifactId>backend-core</artifactId>
  <version>2.1.0-SNAPSHOT</version>
  <packaging>jar</packaging>

  <name>backend-core</name>
  <!--<url>git:releases://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/backend-core.git</url>-->
  <url>git:snapshots://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/backend-core.git</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <additionalparam>-Xdoclint:none</additionalparam>

    <jackson.version>2.9.7</jackson.version>
    <spring.version>5.1.1.RELEASE</spring.version>
    <spring.data.version>2.1.1.RELEASE</spring.data.version>
    <hibernate.version>5.3.7.Final</hibernate.version>
    <sendgrid.version>4.3.0</sendgrid.version>
    <junit.jupiter.version>5.3.1</junit.jupiter.version>
  </properties>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.7.0</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
        </configuration>
      </plugin>
      <!-- JUnit 5 requires Surefire version 2.22.0 or higher -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.22.1</version>
      </plugin>
    </plugins>

    <extensions>
      <extension>
        <groupId>ar.com.synergian</groupId>
        <artifactId>wagon-git</artifactId>
        <version>0.2.5</version>
      </extension>
    </extensions>
  </build>

  <distributionManagement>
    <repository>
      <id>sprintin-backend-core</id>
      <name>Sprint-in Backend Core</name>
      <url>git:releases://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/backend-core.git</url>
    </repository>
    <snapshotRepository>
      <id>sprintin-backend-core</id>
      <name>Sprint-in Backend Core</name>
      <url>git:snapshots://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/backend-core.git</url>
    </snapshotRepository>
  </distributionManagement>

  <pluginRepositories>
    <pluginRepository>
      <id>synergian-repo</id>
      <url>https://raw.github.com/synergian/wagon-git/snapshots</url>
      <!--<url>https://raw.github.com/synergian/wagon-git/releases</url>-->
    </pluginRepository>
  </pluginRepositories>

  <dependencies>
    <!-- JSON -->
    <dependency>
        <groupId>org.json</groupId>
        <artifactId>json</artifactId>
        <version>20170516</version>
    </dependency>

    <dependency>
        <groupId>com.fasterxml.jackson.datatype</groupId>
        <artifactId>jackson-datatype-jdk8</artifactId>
        <version>${jackson.version}</version>
    </dependency>

    <dependency>
        <groupId>com.fasterxml.jackson.datatype</groupId>
        <artifactId>jackson-datatype-jsr310</artifactId>
        <version>${jackson.version}</version>
    </dependency>

    <!-- Hibernate mapping to JSON - especially LazyLoad -->
    <dependency>
      <groupId>com.fasterxml.jackson.datatype</groupId>
      <artifactId>jackson-datatype-hibernate5</artifactId>
      <version>${jackson.version}</version>
    </dependency>

    <!-- Bean Utils -->
    <dependency>
      <groupId>commons-beanutils</groupId>
      <artifactId>commons-beanutils</artifactId>
      <version>1.9.3</version>
    </dependency>

    <!-- Latest Jackson JSON -->
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-core</artifactId>
      <version>${jackson.version}</version>
    </dependency>

    <!-- Latest Jackson JSON -->
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-annotations</artifactId>
      <version>${jackson.version}</version>
    </dependency>

    <!-- Latest Jackson JSON -->
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>${jackson.version}</version>
    </dependency>

    <!-- Apache commons -->
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>3.8.1</version>
    </dependency>

    <!-- Hibernate-->
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate-core</artifactId>
      <version>${hibernate.version}</version>
    </dependency>

    <!-- Hibernate-C3P0 Integration -->
    <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-c3p0</artifactId>
        <version>${hibernate.version}</version>
    </dependency>

    <!-- c3p0 -->
    <dependency>
        <groupId>com.mchange</groupId>
        <artifactId>c3p0</artifactId>
        <version>0.9.5.2</version>
    </dependency>

    <!-- Hibernate Validator -->
    <dependency>
        <groupId>org.hibernate.validator</groupId>
        <artifactId>hibernate-validator</artifactId>
        <version>6.0.2.Final</version>
    </dependency>

    <!-- Google Cloud Storage -->
    <dependency>
      <groupId>com.google.appengine.tools</groupId>
      <artifactId>appengine-gcs-client</artifactId>
      <version>0.8</version>
    </dependency>

    <!-- BCrypt for hashing passwords -->
    <dependency>
      <groupId>org.mindrot</groupId>
      <artifactId>jbcrypt</artifactId>
      <version>0.4</version>
    </dependency>

    <!-- Stream handlers etc. -->
    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
      <version>2.6</version>
    </dependency>

    <!-- MySQL connector-->
    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>5.1.44</version>
    </dependency>

    <!-- Socket factory for local access to the database -->
    <dependency>
        <groupId>com.google.cloud.sql</groupId>
        <artifactId>mysql-socket-factory</artifactId> <!-- mysql-socket-factory-connector-j-6 if using 6.x.x -->
        <version>1.0.4</version>
    </dependency>

    <!-- JDBC connection pool -->
    <dependency>
      <groupId>commons-dbcp</groupId>
      <artifactId>commons-dbcp</artifactId>
      <version>1.4</version>
    </dependency>

    <!-- Holidays -->
    <dependency>
      <groupId>de.jollyday</groupId>
      <artifactId>jollyday</artifactId>
      <version>0.5.3</version>
    </dependency>

    <!-- Handling holidays -->
    <dependency>
      <groupId>net.objectlab.kit</groupId>
      <artifactId>datecalc-common</artifactId>
      <version>1.4.0</version>
    </dependency>
    <dependency>
      <groupId>net.objectlab.kit</groupId>
      <artifactId>datecalc-jdk8</artifactId>
      <version>1.4.0</version>
    </dependency>

    <!--MD5 support-->
    <dependency>
      <groupId>commons-codec</groupId>
      <artifactId>commons-codec</artifactId>
      <version>1.9</version>
    </dependency>

    <!-- Spring ORM -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-orm</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <!-- Spring Web -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-web</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <!-- Spring Context -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>${spring.version}</version>
    </dependency>

    <!-- Spring Data support -->
    <dependency>
      <groupId>org.springframework.data</groupId>
      <artifactId>spring-data-jpa</artifactId>
      <version>${spring.data.version}</version>
    </dependency>

    <!--SendGrid to send emails-->
    <dependency>
      <groupId>com.sendgrid</groupId>
      <artifactId>sendgrid-java</artifactId>
      <version>${sendgrid.version}</version>
    </dependency>

    <!-- http client needed for sendgrid to work inside deferred task -->
    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpclient</artifactId>
      <version>4.5.6</version>
    </dependency>

    <!-- HTTP core -->
    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpcore</artifactId>
      <version>4.4.10</version>
    </dependency>

    <!-- HTTP MIME -->
    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpmime</artifactId>
      <version>4.5.6</version>
    </dependency>

    <!-- Java EE Servlet -->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <version>3.1.0</version>
    </dependency>

    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-api</artifactId>
      <version>${junit.jupiter.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-params</artifactId>
      <version>${junit.jupiter.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-engine</artifactId>
      <version>${junit.jupiter.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
      <version>1.4.196</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <version>${spring.version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <repositories>
    <repository>
      <id>jvnet-nexus-snapshots</id>
      <name>jvnet-nexus-snapshots</name>
      <url>https://maven.java.net/content/repositories/snapshots/</url>
    </repository>
  </repositories>
</project>
