<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.sprintin.core</groupId>
  <artifactId>backend-core</artifactId>
  <version>1.0.1-SNAPSHOT</version>
  <packaging>jar</packaging>

  <name>primefaces-core</name>
  <!--<url>git:releases://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/backend-core.git</url>-->
  <url>git:snapshots://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/backend-core.git</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.1</version>
        <configuration>
          <source>1.7</source>
          <target>1.7</target>
        </configuration>
      </plugin>
    </plugins>

    <extensions>
      <extension>
        <groupId>ar.com.synergian</groupId>
        <artifactId>wagon-git</artifactId>
        <version>0.2.5</version>
      </extension>
    </extensions>
  </build>

  <distributionManagement>
    <repository>
      <id>sprintin-primefaces-core</id>
      <name>Sprint-in Primefaces Core</name>
      <url>git:releases://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/backend-core.git</url>
    </repository>
    <snapshotRepository>
      <id>sprintin-primefaces-core</id>
      <name>Sprint-in Primefaces Core</name>
      <url>git:snapshots://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/backend-core.git</url>
    </snapshotRepository>
  </distributionManagement>

  <pluginRepositories>
    <pluginRepository>
      <id>synergian-repo</id>
      <url>https://raw.github.com/synergian/wagon-git/snapshots</url>
      <!--<url>https://raw.github.com/synergian/wagon-git/releases</url>-->
    </pluginRepository>
  </pluginRepositories>

  <dependencies>

    <!-- Hibernate-->
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate-entitymanager</artifactId>
      <version>4.2.0.Final</version>
    </dependency>

    <!-- MySQL connector-->
    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>5.1.37</version>
    </dependency>

    <!-- JDBC connection pool -->
    <dependency>
      <groupId>commons-dbcp</groupId>
      <artifactId>commons-dbcp</artifactId>
      <version>1.4</version>
    </dependency>

    <!--MD5 support-->
    <dependency>
      <groupId>commons-codec</groupId>
      <artifactId>commons-codec</artifactId>
      <version>1.9</version>
    </dependency>

    <!-- Spring ORM -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-orm</artifactId>
      <version>4.3.4.RELEASE</version>
    </dependency>

    <!-- Spring Web -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-web</artifactId>
      <version>4.3.4.RELEASE</version>
    </dependency>

    <!--SendGrid to send emails-->
    <dependency>
      <groupId>com.sendgrid</groupId>
      <artifactId>sendgrid-java</artifactId>
      <version>2.2.2</version>
    </dependency>

    <!-- http client needed for sendgrid to work inside deferred task -->
    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpclient</artifactId>
      <version>4.5.2</version>
    </dependency>

    <!-- Java EE Servlet -->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <version>3.1.0</version>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <repositories>
    <repository>
      <id>jvnet-nexus-snapshots</id>
      <name>jvnet-nexus-snapshots</name>
      <url>https://maven.java.net/content/repositories/snapshots/</url>
    </repository>
  </repositories>
</project>
